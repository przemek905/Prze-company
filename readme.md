## **Prze-Company**

Simple App created for private use to keep invoices and company budget in order.

**Mongo DB**

Steps to run Mongo DB locally:
1. docker pull mongo
2. docker run -d -p 27017-27019:27017-27019 --name mongodb mongo

Tu run App with Mongo on Docker:
docker-compose up

To rebuild docker compose:
**docker-compose-up --build**

`Swagger docs:`
http://localhost:8080/swagger-ui/


