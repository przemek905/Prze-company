package pl.pluszkiewicz.przecompany.exceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RestError {
    private String errorMessage;
}
