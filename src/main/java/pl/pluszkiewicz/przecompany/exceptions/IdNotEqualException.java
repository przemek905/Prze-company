package pl.pluszkiewicz.przecompany.exceptions;

public class IdNotEqualException extends RuntimeException {

    public IdNotEqualException(String message) {
        super(message);
    }

    public IdNotEqualException(String message, Throwable cause) {
        super(message, cause);
    }
}
